data = [];
labels = [];

for i = 1:200
    imageA = imread("A\A-" + (i-1) + ".png");
    imageB = imread("B\B-" + (i-1) + ".png");
    imageC = imread("C\C-" + (i-1) + ".png");
    imageD = imread("D\D-" + (i-1) + ".png");
    imageE = imread("E\E-" + (i-1) + ".png");
    vectorA = reshape(imageA, 1, []);
    vectorB = reshape(imageB, 1, []);
    vectorC = reshape(imageC, 1, []);
    vectorD = reshape(imageD, 1, []);
    vectorE = reshape(imageE, 1, []);
    data = [vectorA; data];
    data = [vectorB; data];
    data = [vectorC; data];
    data = [vectorD; data];
    data = [vectorE; data];
    labels = [labels; 1];
    labels = [labels; 2];
    labels = [labels; 3];
    labels = [labels; 4];
    labels = [labels; 5];
end

save("input.mat", 'data', 'labels')